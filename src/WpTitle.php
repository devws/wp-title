<?php

namespace DevWs\WpTitle;

class WpTitle {
	public function __construct() {
		add_action( 'edit_form_before_permalink', [ $this, 'titleElements' ] );
		add_action( 'save_post', [ $this, 'save' ], 10, 3 );
		add_action( 'admin_enqueue_scripts', [ $this, 'scripts' ] );
		add_action( 'wp_ajax_wp_title_get_post', [ $this, 'getPost' ] );
		add_filter( 'wp_title', [ $this, 'titleFilter' ], 10, 3 );
		add_filter( 'document_title_parts', [ $this, 'documentTitleParts' ] );
		add_filter( 'the_title', [ self::class, 'theTitleFilter' ], 10, 2 );
	}

	/**
	 * head의 title 태그에 title_tag, subtitle_head, subtitle_tail 사용할 수 있도록 한다.
	 *
	 * @param string $title
	 * @param string $sep
	 * @param string $seplocation
	 *
	 * @return string
	 */
	public function titleFilter( string $title, string $sep, string $seplocation ): string {
		if ( is_singular() ) {
			return self::getTitleText() . " $sep ";
		}

		return $title;
	}

	/**
	 * @param \WP_Post|int $post
	 *
	 * @return string
	 */
	public static function getTitleText( $post = null ): string {
		$post = get_post( $post );

        $custom_title_text = get_post_meta( get_the_ID(), 'custom_title_text', true );
        if ($custom_title_text) {
            return $custom_title_text;
        }

		$title_text = '';

		$title_tag     = get_post_meta( $post->ID, 'title_tag', true );
		$subtitle_head = get_post_meta( $post->ID, 'subtitle_head', true );
		$title         = $post->post_title;
		$subtitle_tail = get_post_meta( $post->ID, 'subtitle_tail', true );

		$title_text .= ( $title_tag ) ? "[{$title_tag}] " : '';
		$title_text .= ( $subtitle_head ) ? "{$subtitle_head}: " : '';
		$title_text .= ( $title ) ?: "(제목 없음)";
		$title_text .= ( $subtitle_tail ) ? " — {$subtitle_tail}" : '';

        $title_text = strip_tags($title_text);
        $title_text = str_replace("\r\n", " ", $title_text);

		return $title_text;
	}
	/**
	 * @param \WP_Post|int $post
	 *
	 * @return string
	 */
	public static function getTitleTextWithoutTitleTag( $post = null ): string {
		$post = get_post( $post );

		$title_text = '';

		$subtitle_head = get_post_meta( $post->ID, 'subtitle_head', true );
		$title         = $post->post_title;
		$subtitle_tail = get_post_meta( $post->ID, 'subtitle_tail', true );

		$title_text .= ( $subtitle_head ) ? "{$subtitle_head}: " : '';
		$title_text .= ( $title ) ?: "(제목 없음)";
		$title_text .= ( $subtitle_tail ) ? " — {$subtitle_tail}" : '';

		return $title_text;
	}

	public function getPost() {
		$args = json_decode( file_get_contents( 'php://input' ) );
		$post = get_post( $args->id );
		echo json_encode( $post );
		die();
	}

	public function titleElements() {
		$title_tag     = get_post_meta( get_the_ID(), 'title_tag', true );
		$subtitle_head = get_post_meta( get_the_ID(), 'subtitle_head', true );
		$subtitle_tail = get_post_meta( get_the_ID(), 'subtitle_tail', true );
		$custom_title_text = get_post_meta( get_the_ID(), 'custom_title_text', true );
		include 'titles.php';
	}

	public function save( $post_ID, $post, $update ) {
		if ( ! empty( $_POST['ws_title'] ) ) {
			foreach ( $_POST['ws_title'] as $k => $v ) {
				update_post_meta( $post_ID, $k, $v );
			}
		}
	}

	public function scripts( $hook ) {
		if ( in_array( $hook, [ 'post.php', 'post-new.php' ] ) ) {
			$dir = realpath(__DIR__ . '/..');
			$uri = str_replace(ABSPATH, '', $dir);
			$src     = site_url($uri . '/js/app.js');
			$version = filemtime( $dir . '/js/app.js' );
			wp_enqueue_script( 'ws-title', $src, [], $version, true );
		}
	}

    /**
     * 제목만 줄바꿈 적용해서 가져올 때 사용
     * @param $post
     * @return string
     * @deprecated getOnlyTitleWithBr()를 사용하세요.
     */
    public static function getTheTitle( $post = null ): string
    {
        return self::getOnlyTitleWithBr($post);
    }

    /**
     * 제목만 줄바꿈 적용해서 가져올 때 사용
     * @param $post
     * @return string
     */
    public static function getOnlyTitleWithBr( $post = null ): string
    {
        $post = get_post( $post );
        remove_filter('the_title', [ self::class, 'theTitleFilter' ]);
        $title = nl2br(the_title('', '', false));
        add_filter( 'the_title', [ self::class, 'theTitleFilter' ], 10, 2 );
        return $title;
    }

    /**
     * 제목만 줄바꿈 적용해서 출력할 때 사용
     * @deprecated onlyTitleWithBr을 사용하세요.
     * @param $post
     * @return string
     */
    public static function theTitle( $post = null ): void
    {
        self::onlyTitleWithBr($post);
    }

    /**
     * 제목만 줄바꿈 적용해서 출력할 때 사용
     * @param $post
     * @return string
     */
    public static function onlyTitleWithBr( $post = null ): void
    {
        echo self::getOnlyTitleWithBr($post);
    }

    public function documentTitleParts(array $title): array
    {
        if (!is_singular()) {
            return $title;
        }
        return [
            'title' => self::getTitleText(),
            'site' => $title['site'],
        ];
    }

    public static function theTitleFilter($post_title, $post_id): string
    {
        $post_type_object = get_post_type_object(get_post_type($post_id));
        $post_type_has_archive = ($post_type_object && $post_type_object->has_archive);

        if (!$post_type_has_archive) {
            return $post_title;
        }
        return self::getTitleText($post_id);
    }
}
