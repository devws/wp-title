import axios from 'axios';

function replaceWithTitleTextarea(title) {
    const titleWrapEl = document.getElementById('titlewrap');
    const textareaEl = document.createElement('textarea');
    textareaEl.classList.add('widefat');
    textareaEl.id = 'title';
    textareaEl.rows = 3;
    textareaEl.name = 'post_title';
    textareaEl.spellcheck = true;
    textareaEl.autocomplete = 'off';
    textareaEl.innerHTML = title;
    textareaEl.style.height = 'auto';
    textareaEl.placeholder = '제목을 입력하세요';
    titleWrapEl.remove();
    document.querySelector('.js-title-position').append(textareaEl)
    document.querySelector('.js-ws-titles').hidden = false;
}

function titleProcess() {
    // 제목을 textarea로 변경.
    const id = document.getElementById('post_ID').value;
    axios.post(ajaxurl, {
        id: id
    }, {
        params: {
            action: 'wp_title_get_post'
        }
    }).then(res => {
        if (adminpage === 'post-new-php') {
            replaceWithTitleTextarea('');
            const promptTextLabel = document.getElementById('title-prompt-text');
            if (promptTextLabel) {
                promptTextLabel.remove();
            }
        } else {
            replaceWithTitleTextarea(res.data.post_title);
        }
    }).catch(error => {
        alert(error);
    });
}

document.addEventListener('DOMContentLoaded', () => {
    titleProcess();
});
