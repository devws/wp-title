<?php
/**
 * @var string $title_tag
 * @var string $subtitle_head
 * @var string $subtitle_tail
 */
?>
<table class="form-table  js-ws-titles" hidden>
    <tbody>
    <tr>
        <th style="text-align: right; width: 5em;"><label for="ws_title[title_tag]">말머리</label></th>
        <td>
            <input type="text" class="large-text" name="ws_title[title_tag]" id="ws_title[title_tag]" value="<?= $title_tag ?>">
        </td>
    </tr>
    <tr>
        <th style="text-align: right; width: 5em;"><label for="ws_title[subtitle_head]">윗부제</label></th>
        <td>
            <input type="text" class="large-text" name="ws_title[subtitle_head]" id="ws_title[subtitle_head]" value="<?= $subtitle_head ?>">
        </td>
    </tr>
    <tr>
        <th style="text-align: right; width: 5em;"><label for="post_title">제목</label></th>
        <td class="js-title-position"></td>
    </tr>
    <tr>
        <th style="text-align: right; width: 5em;"><label for="ws_title[subtitle_tail]">아랫부제</label></th>
        <td>
            <input type="text" class="large-text" name="ws_title[subtitle_tail]" id="ws_title[subtitle_tail]" value="<?= $subtitle_tail ?>">
        </td>
    </tr>
    <tr>
        <th style="text-align: right; width: 5em;"><label for="ws_title[subtitle_tail]">검색엔진, SNS용<br>사용자 정의 제목</label></th>
        <td>
            <input type="text" class="large-text" name="ws_title[custom_title_text]" id="ws_title[custom_title_text]"
                   value="<?= $custom_title_text ?>" placeholder="비워 두면 기본값이 적용됩니다.">
        </td>
    </tr>
    </tbody>
</table>
